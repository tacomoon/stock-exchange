package net.tacomoon.exchange;

import net.tacomoon.model.Client;
import net.tacomoon.model.Order;
import org.assertj.core.api.Condition;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static net.tacomoon.model.Operation.Buy;
import static net.tacomoon.model.Operation.Sell;
import static net.tacomoon.model.Resource.A;
import static net.tacomoon.model.Resource.B;
import static net.tacomoon.model.Resource.C;
import static net.tacomoon.model.Resource.D;
import static org.assertj.core.api.Assertions.assertThat;

class ExchangeTest {
    private final Map<String, Client> clients = Map.of(
            "Alice", new Client("Alice", 100, Map.of(A, 10L, B, 10L)),
            "Bob", new Client("Bob", 100, Map.of(B, 10L, C, 10L)),
            "Charlie", new Client("Charlie", 100, Map.of(C, 10L, D, 10L)),
            "Dave", new Client("Dave", 100, Map.of(D, 10L, A, 10L))
    );

    @Test
    void put_first_buy_order() {
        final Exchange exchange = new SimpleExchange(clients.values());
        final List<Order> buyOrders = List.of(
                new Order("Alice", Buy, A, 10, 1),
                new Order("Bob", Buy, B, 20, 2),
                new Order("Charlie", Buy, C, 30, 3),
                new Order("Alice", Buy, D, 40, 4)
        );

        buyOrders.forEach(exchange::put);

        assertThat(exchange.getOrders())
                .as("Expecting exchange to contain a buy orders")
                .containsExactlyEntriesOf(buyOrders.stream().collect(groupingBy(Order::getResource)));
    }

    @Test
    void put_first_sell_order() {
        final Exchange exchange = new SimpleExchange(clients.values());
        final List<Order> sellOrders = List.of(
                new Order("Alice", Sell, A, 10, 1),
                new Order("Bob", Sell, B, 20, 2),
                new Order("Charlie", Sell, C, 30, 3),
                new Order("Dave", Sell, D, 40, 4)
        );

        sellOrders.forEach(exchange::put);

        assertThat(exchange.getOrders())
                .as("Expecting exchange to contain a sell orders")
                .containsExactlyEntriesOf(sellOrders.stream().collect(groupingBy(Order::getResource)));
    }

    @Test
    void exact_match_buy_and_sell_order() {
        final Exchange exchange = new SimpleExchange(clients.values());

        final Order buyOrder = new Order("Alice", Buy, B, 10, 2);
        final Order sellOrder = new Order("Bob", Sell, B, 10, 2);

        exchange.put(buyOrder);
        exchange.put(sellOrder);

        final SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(exchange.getOrders())
                .as("Expecting exchange not to contains buy and sell order")
                .has(new Condition<>(
                        actual -> !actual.get(buyOrder.getResource()).contains(buyOrder)
                                && !actual.get(sellOrder.getResource()).contains(sellOrder),
                        "Map does not contains buy and sell orders")
                );

        final Map<String, Client> actualClients = exchange.getClients().stream()
                .collect(toMap(Client::getName, identity()));

        assertions.assertThat(actualClients.get(buyOrder.getClient()))
                .as("Expecting buyer to receive resource and paid")
                .has(new Condition<>(
                        buyer -> buyer.getBalance() == clients.get(buyer.getName()).getBalance() - sellOrder.getPrice() * sellOrder.getAmount()
                                && buyer.getResourceMap().get(buyOrder.getResource()) == clients.get(buyer.getName()).getResourceMap().getOrDefault(buyOrder.getResource(), 0L) + sellOrder.getAmount(),
                        "Buyer received resource and paid"
                ));

        assertions.assertThat(actualClients.get(sellOrder.getClient()))
                .as("Expecting seller to reduce at resource and got paid")
                .has(new Condition<>(
                        seller -> seller.getBalance() == clients.get(seller.getName()).getBalance() + sellOrder.getPrice() * sellOrder.getAmount()
                                && seller.getResourceMap().get(sellOrder.getResource()) == clients.get(seller.getName()).getResourceMap().get(sellOrder.getResource()) - sellOrder.getAmount(),
                        "Seller reduced at resource and received payment"
                ));

        assertions.assertAll();
    }

    @Test
    void buy_and_sell_order_from_same_client_does_not_match() {
        final Exchange exchange = new SimpleExchange(clients.values());

        final Order buyOrder = new Order("Bob", Buy, B, 10, 2);
        final Order sellOrder = new Order("Bob", Sell, B, 10, 2);

        exchange.put(buyOrder);
        exchange.put(sellOrder);

        assertThat(exchange.getOrders())
                .as("Expecting exchange to contain buy and sell orders from same client")
                .has(new Condition<>(
                        actual -> actual.get(buyOrder.getResource()) != null
                                && actual.get(buyOrder.getResource()).contains(buyOrder)
                                && actual.get(buyOrder.getResource()).contains(sellOrder),
                        "Map does not contains buy and sell orders from same client"
                ));
    }

    @Test
    void buy_order_does_not_match_when_client_has_not_enough_balance_to_buy() {
        final Exchange exchange = new SimpleExchange(clients.values());

        final Order buyOrder = new Order("Alice", Buy, C, 101, 1);
        final Order sellOrder = new Order("Charlie", Sell, C, 101, 1);

        exchange.put(buyOrder);
        exchange.put(sellOrder);

        final SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(exchange.getOrders())
                .as("Expecting exchange to contain buy order when the client has not enough balance to buy resource")
                .has(new Condition<>(
                        actual -> actual.get(buyOrder.getResource()) != null
                                && actual.get(buyOrder.getResource()).contains(buyOrder),
                        "Map does not contains buy order when client balance not enough to buy resource"
                ));

        assertions.assertThat(exchange.getClients())
                .as("Expecting all clients balance not to move below zero")
                .has(new Condition<>(
                        actual -> actual.stream().allMatch(client -> client.getBalance() > 0),
                        "Client balance should not move below zero"
                ));

        assertions.assertAll();
    }

    @Test
    void sell_order_does_not_match_when_client_has_not_enough_resources_to_sell() {
        final Exchange exchange = new SimpleExchange(clients.values());

        final Order buyOrder = new Order("Alice", Buy, D, 1, 20);
        final Order sellOrder = new Order("Dave", Sell, D, 1, 20);

        exchange.put(buyOrder);
        exchange.put(sellOrder);

        final SoftAssertions assertions = new SoftAssertions();

        assertions.assertThat(exchange.getOrders())
                .as("Expecting exchange to contains sell order when the client has not enough resource to sell")
                .has(new Condition<>(
                        actual -> actual.get(sellOrder.getResource()) != null
                                && actual.get(sellOrder.getResource()).contains(sellOrder),
                        "Map does not contains sell order when client has not enough resource to sell"
                ));

        assertions.assertThat(exchange.getClients())
                .as("Expecting all clients resources amount not to move below zero")
                .has(new Condition<>(
                        actual -> actual.stream().allMatch(client ->
                                client.getResourceMap().entrySet().stream()
                                        .allMatch(entry -> entry.getValue() > 0)
                        ),
                        "Clients resources balance should not move below zero"
                ));

        assertions.assertAll();
    }
}