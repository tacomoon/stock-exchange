package net.tacomoon.exchange;

import net.tacomoon.model.Client;
import net.tacomoon.model.Operation;
import net.tacomoon.model.Order;
import net.tacomoon.model.Resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static java.util.stream.Collectors.toMap;

public class SimpleExchange implements Exchange {
    private final Map<String, Client> clientMap;
    private final Map<Resource, LinkedList<Order>> queueMap;

    public SimpleExchange(Collection<Client> clients) {
        clientMap = clients.stream().collect(toMap(Client::getName, Client::copy));
        queueMap = new HashMap<>();
    }

    @Override
    public List<Client> getClients() {
        return new ArrayList<>(clientMap.values());
    }

    @Override
    public Map<Resource, List<Order>> getOrders() {
        return queueMap.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, entry -> new ArrayList<>(entry.getValue())));
    }

    @Override
    public void put(Order order) {
        queueMap.computeIfAbsent(order.getResource(), resource -> new LinkedList<>())
                .addFirst(Order.copy(order));

        processQueue(order.getResource());
    }

    private void processQueue(Resource resource) {
        boolean matched;

        do {
            matched = false;
            if (queueMap.get(resource).size() < 2) {
                break;
            }

            final LinkedList<Order> orders = queueMap.get(resource);

            final Iterator<Order> iterator = orders.iterator();
            final Order order = iterator.next();

            while (iterator.hasNext()) {
                final Order matching = iterator.next();

                if (matchOrder(order, matching)) {
                    System.out.printf("Order %s matches %s%n", order, matching);

                    orders.remove(order);
                    orders.remove(matching);

                    final String buyer = order.getOperation() == Operation.Buy
                            ? order.getClient()
                            : matching.getClient();
                    final String seller = order.getOperation() == Operation.Sell
                            ? order.getClient()
                            : matching.getClient();

                    matched = transaction(buyer, seller, order.getResource(), order.getPrice(), order.getAmount());
                    break;
                }
            }
        } while (matched);
    }

    private boolean matchOrder(Order left, Order right) {
        return left.getOperation() != right.getOperation()
                && left.getPrice() == right.getPrice()
                && left.getAmount() == right.getAmount();
    }

    private boolean transaction(String buyerName, String sellerName, Resource resource, long price, long amount) {
        System.out.printf("Processing transaction: buyer %s, seller: %s, " +
                "resource: %s, price %s, amount: %s%n", buyerName, sellerName, resource, price, amount);

        if (!clientMap.containsKey(buyerName) || !clientMap.containsKey(sellerName)) {
            throw new IllegalStateException(format("Unable to proceed transaction, unknown buyer %s or seller %s%n", buyerName, sellerName));
        }

        clientMap.get(buyerName).buy(resource, amount, price);
        clientMap.get(sellerName).sell(resource, amount, price);

        return true;
    }
}
