package net.tacomoon.exchange;

import net.tacomoon.model.Client;
import net.tacomoon.model.Order;
import net.tacomoon.model.Resource;

import java.util.List;
import java.util.Map;

public interface Exchange {

    void put(Order order);

    List<Client> getClients();

    Map<Resource, List<Order>> getOrders();
}
