package net.tacomoon.io;

import net.tacomoon.model.Client;
import net.tacomoon.model.Operation;
import net.tacomoon.model.Order;
import net.tacomoon.model.Resource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.lang.Long.parseLong;
import static java.util.stream.Collectors.toList;

public final class IOUtils {
    private IOUtils() {
    }

    public static Stream<Order> streamOrders(File ordersFile) {
        try {
            return Files.lines(ordersFile.toPath())
                    .map(IOUtils::parseOrder);
        } catch (IOException ex) {
            throw new RuntimeException("Unable to read orders file", ex);
        }
    }

    public static List<Client> readClients(File clientsFile) {
        try {
            return Files.lines(clientsFile.toPath())
                    .map(IOUtils::parseClient)
                    .collect(toList());
        } catch (IOException ex) {
            throw new RuntimeException("Unable to read clients file", ex);
        }
    }

    public static void writeClients(List<Client> clients, File output) throws IOException {
        try (final BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {
            for (Client client : clients) {
                writer.write(formatClient(client));
                writer.newLine();
            }
        }
    }

    private static String formatClient(Client client) {
        return client.getName() + '\t' +
                client.getBalance() + '\t' +
                client.getResourceMap().getOrDefault(Resource.A, 0L) + '\t' +
                client.getResourceMap().getOrDefault(Resource.B, 0L) + '\t' +
                client.getResourceMap().getOrDefault(Resource.C, 0L) + '\t' +
                client.getResourceMap().getOrDefault(Resource.D, 0L) + '\t';
    }

    private static Client parseClient(final String input) {
        final String[] splits = input.split("\\s+", 6);
        if (splits.length != 6) {
            throw new RuntimeException("Unable to parse client. Input: '" + input + "'");
        }

        final String name = splits[0];
        final long balance = parseLong(splits[1]);
        final Map<Resource, Long> resources = Map.of(
                Resource.A, parseLong(splits[2]),
                Resource.B, parseLong(splits[3]),
                Resource.C, parseLong(splits[4]),
                Resource.D, parseLong(splits[5])
        );

        return new Client(name, balance, resources);
    }

    private static Order parseOrder(final String input) {
        final String[] splits = input.split("\\s+", 5);
        if (splits.length != 5) {
            throw new RuntimeException("Unable to parse order. Input: '" + input + "'");
        }

        final String client = splits[0];
        final Operation operation = parseOperation(splits[1]);
        final Resource resource = parseResource(splits[2]);
        final long price = parseLong(splits[3]);
        final long amount = parseLong(splits[4]);

        return new Order(client, operation, resource, price, amount);
    }

    private static Resource parseResource(String input) {
        try {
            return Resource.valueOf(input);
        } catch (IllegalStateException ex) {
            throw new RuntimeException("Unable to parse order. Incorrect resource: '" + input + "'");
        }
    }

    private static Operation parseOperation(String input) {
        switch (input) {
            case "b":
                return Operation.Buy;
            case "s":
                return Operation.Sell;
            default:
                throw new RuntimeException("Unable to parse order. Incorrect operation: '" + input + "'");
        }
    }
}
