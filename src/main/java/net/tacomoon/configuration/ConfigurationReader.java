package net.tacomoon.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ConfigurationReader {
    private ConfigurationReader() {
    }

    public static Configuration readConfiguration(List<String> args) {
        final Map<String, String> params = parseArgs(args);

        final Configuration.Builder builder = new Configuration.Builder();
        if (params.containsKey("clients")) {
            builder.clientsFile(params.get("clients"));
        }
        if (params.containsKey("orders")) {
            builder.ordersFile(params.get("orders"));
        }
        if (params.containsKey("output")) {
            builder.outputFile(params.get("output"));
        }

        return builder.build();
    }

    private static Map<String, String> parseArgs(List<String> args) {
        final HashMap<String, String> params = new HashMap<>();

        int i;
        for (i = 0; i < args.size(); i++) {
            if (args.get(i).startsWith("--")) break;
        }

        String name = null;
        StringBuilder value = new StringBuilder();
        for (; i < args.size(); i++) {
            final String argument = args.get(i);
            if (argument.startsWith("--")) {
                if (name != null) {
                    params.put(name, value.toString().trim());
                    value = new StringBuilder();
                }

                name = argument.substring(2);
            } else {
                value.append(argument).append(" ");
            }
        }

        params.put(name, value.toString().trim());

        return params;
    }
}
