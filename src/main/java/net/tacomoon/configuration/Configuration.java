package net.tacomoon.configuration;

import java.io.File;
import java.net.URISyntaxException;

public class Configuration {
    private File clientsFile;
    private File ordersFile;
    private File outputFile;

    private Configuration() {
    }

    public File getClientsFile() {
        return clientsFile;
    }

    public File getOrdersFile() {
        return ordersFile;
    }

    public File getOutputFile() {
        return outputFile;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "clientsFile=" + clientsFile +
                ", ordersFile=" + ordersFile +
                ", outputFile=" + outputFile +
                '}';
    }

    @SuppressWarnings("UnusedReturnValue")
    public static class Builder {
        private final Configuration configuration;

        public Builder() {
            this.configuration = new Configuration();
        }

        public Builder clientsFile(String clientsFilename) {
            this.configuration.clientsFile = new File(clientsFilename);
            return this;
        }

        public Builder ordersFile(String ordersFilename) {
            this.configuration.ordersFile = new File(ordersFilename);
            return this;
        }

        public Builder outputFile(String outputFilename) {
            this.configuration.outputFile = new File(outputFilename);
            return this;
        }

        public Configuration build() {
            if (configuration.clientsFile == null) {
                configuration.clientsFile = getDefaultLocationFile("clients.txt");
            }
            if (configuration.ordersFile == null) {
                configuration.ordersFile = getDefaultLocationFile("orders.txt");
            }
            if (configuration.outputFile == null) {
                configuration.outputFile = new File("output.txt");
            }

            return configuration;
        }

        private File getDefaultLocationFile(String filename) {
            final File currentDirectoryFile = new File(filename);
            if (currentDirectoryFile.exists()) {
                return currentDirectoryFile;
            }

            try {
                final File resourceFile = new File(ClassLoader.getSystemResource(filename).toURI());
                if (resourceFile.exists()) {
                    return resourceFile;
                }
            } catch (URISyntaxException ex) {
                throw new IllegalStateException(ex);
            }

            throw new IllegalStateException("Unable to locate '" + filename + "', please specify path manually");
        }
    }
}
