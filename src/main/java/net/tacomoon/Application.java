package net.tacomoon;

import net.tacomoon.configuration.Configuration;
import net.tacomoon.exchange.Exchange;
import net.tacomoon.exchange.SimpleExchange;
import net.tacomoon.model.Client;
import net.tacomoon.model.Order;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static net.tacomoon.configuration.ConfigurationReader.readConfiguration;
import static net.tacomoon.io.IOUtils.readClients;
import static net.tacomoon.io.IOUtils.streamOrders;
import static net.tacomoon.io.IOUtils.writeClients;

public class Application {
    public static void main(String[] args) throws Exception {
        final Configuration configuration = readConfiguration(Arrays.asList(args));

        final List<Client> clients = readClients(configuration.getClientsFile());
        final Exchange exchange = new SimpleExchange(clients);

        final Stream<Order> stream = streamOrders(configuration.getOrdersFile());
        stream.forEach(exchange::put);

        final Stream<Client> result = exchange.getClients().stream()
                .sorted(comparing(Client::getName));

        writeClients(result.collect(toList()), configuration.getOutputFile());
    }
}
