package net.tacomoon.model;

import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toMap;

public class Client {
    private final String name;
    private final Map<Resource, Long> resourceMap;

    private long balance;

    @SuppressWarnings("BoxingBoxedValue")
    public Client(String name, long balance, Map<Resource, Long> resourceMap) {
        this.name = name;
        this.balance = balance;
        this.resourceMap = resourceMap.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, entry -> Long.valueOf(entry.getValue())));
    }

    public static Client copy(Client other) {
        return new Client(
                other.getName(),
                other.getBalance(),
                other.resourceMap
        );
    }

    public String getName() {
        return name;
    }

    public long getBalance() {
        return balance;
    }

    @SuppressWarnings("BoxingBoxedValue")
    public Map<Resource, Long> getResourceMap() {
        return resourceMap.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, entry -> Long.valueOf(entry.getValue())));
    }

    // TODO[EG]: check if client's balance is enough
    public void buy(Resource resource, long amount, long price) {
        balance -= amount * price;

        resourceMap.put(resource, resourceMap.getOrDefault(resource, 0L) + amount);
    }

    // TODO[EG]: check if client's have enough resource
    public void sell(Resource resource, long amount, long price) {
        balance += amount * price;

        // No need to check for negative resource balance
        resourceMap.put(resource, resourceMap.getOrDefault(resource, 0L) - amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (balance != client.balance) return false;
        if (!Objects.equals(name, client.name)) return false;
        return Objects.equals(resourceMap, client.resourceMap);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + resourceMap.hashCode();
        result = 31 * result + (int) (balance ^ (balance >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", balance=" + balance +
                ", resourceMap=" + resourceMap +
                '}';
    }
}
