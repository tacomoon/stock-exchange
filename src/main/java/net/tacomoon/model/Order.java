package net.tacomoon.model;

import java.util.Objects;

public class Order implements Cloneable {

    private final String client;
    private final Operation operation;
    private final Resource resource;
    private final long price;
    private final long amount;

    public Order(String client, Operation operation, Resource resource, long price, long amount) {
        this.client = client;
        this.operation = operation;
        this.resource = resource;
        this.price = price;
        this.amount = amount;
    }

    public static Order copy(Order other) {
        return new Order(
                other.getClient(),
                other.getOperation(),
                other.getResource(),
                other.getPrice(),
                other.getAmount()
        );
    }

    public String getClient() {
        return client;
    }

    public Operation getOperation() {
        return operation;
    }

    public Resource getResource() {
        return resource;
    }

    public long getPrice() {
        return price;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (price != order.price) return false;
        if (amount != order.amount) return false;
        if (!Objects.equals(client, order.client)) return false;
        if (operation != order.operation) return false;
        return resource == order.resource;
    }

    @Override
    public int hashCode() {
        int result = client != null ? client.hashCode() : 0;
        result = 31 * result + (operation != null ? operation.hashCode() : 0);
        result = 31 * result + (resource != null ? resource.hashCode() : 0);
        result = 31 * result + (int) (price ^ (price >>> 32));
        result = 31 * result + (int) (amount ^ (amount >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "client='" + client + '\'' +
                ", operation=" + operation +
                ", resource=" + resource +
                ", price=" + price +
                ", amount=" + amount +
                '}';
    }
}
